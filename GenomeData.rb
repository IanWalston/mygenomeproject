str = File.read('iangenome.txt')
puts "Hello"
puts "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-"

split_str = str.split(" ")

my_genome_array = []
i = 0
subarr = []

split_str.each {|ele|
	i+=1
	subarr << ele

	if i == 4 
		i=0
		my_genome_array << subarr
		subarr = []
	end
	}

nucleotides = "ACTG"
homozygous_genotypes_array = []
heterozygous_genotypes_array = []
nucleotides.each_char {|ele1| 
 nucleotides.each_char {|ele2|	
  	homozygous_genotypes_array<<(ele1 + ele2) if ele1 == ele2	
  	heterozygous_genotypes_array<<(ele1 + ele2) if ele1 != ele2}}



def chromosome_count(genome_array)    # This method counts the number of SNPs for each chromosome 

	ccount = Hash.new(0)

	genome_array.each {|entry| ccount[entry[1]] += 1}
	ccount.each {|chromosome, count| puts "Chromosome " + chromosome.to_s + " has " + count.to_s + " SNP data points" }
	return ccount
end



def genotype_count(genome_array) # This method counts the number of heterozygous, homozygous, haploid, or missing SNPs

	gcount = Hash.new(0)

	nucleotides = "ACTG"
    homozygous_genotypes_array = []
    heterozygous_genotypes_array = []
    nucleotides.each_char {|ele1| 
    nucleotides.each_char {|ele2|	
  	homozygous_genotypes_array<<(ele1 + ele2) if ele1 == ele2	
  	heterozygous_genotypes_array<<(ele1 + ele2) if ele1 != ele2}}

	genome_array.each {|entry|
	
	nucleotides = "ACGT"



		if heterozygous_genotypes_array.include?(entry[3]) 
			gcount["Heterozygous"] += 1 
		elsif homozygous_genotypes_array.include?(entry[3])
			gcount["Homozygous"] += 1
		elsif nucleotides.include?(entry[3])
			gcount["Haploid"] += 1  
		else
			gcount["Missing"] += 1 
		end
		}

	gcount.each {|genotype, count| puts "There are " + count.to_s + " " + genotype + " data points"}


end


chromosome_count(my_genome_array)

puts "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-"

genotype_count(my_genome_array)
